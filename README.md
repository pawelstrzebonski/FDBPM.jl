# FDBPM

## About

`FDBPM` is a package implementing the finite-difference beam propagation
method for simulating the propagation of electromagnetic fields.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDBPM.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for FDBPM.jl](https://pawelstrzebonski.gitlab.io/FDBPM.jl/).
