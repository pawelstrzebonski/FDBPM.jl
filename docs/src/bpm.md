# Simple Beam Propagation Method

Here we implement the simple FD-BPM algorithm for scalar fields in
isotropic media.

```@autodocs
Modules = [FDBPM]
Pages   = ["bpm.jl"]
```
