# Example Usage

We'll start with a simple 2D beam propagation problem (that is, propagation
of a 1D beam in a 2D space). We start by defining the index structure
`n` through which the beam will propagate and the incident field `phi1`.
The field sampling `DeltaX` and `DeltaZ` coefficients
(for the transverse direction
and the direction of propagation respectively) are unit-less
wavelength-normalized constants (that is,
``\Delta X=\frac{\Delta x}{\lambda_0}``).

```@example bpm2d
import Plots: plot, heatmap
import Plots: savefig # hide

DeltaX=0.05
DeltaZ=0.05
x=-6:DeltaX:6
z=0:DeltaZ:6
Nx, Nz=length.([x, z])
n=[x^2+(z-3)^2<=1 ? 3.0 : 1.0 for x=x, z=z]
phi1=[abs(x)<=1.5 ? 1.0 : 0.0 for x=x]

plot(
	heatmap(z, x, n,
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Index Structure",
	),
	plot(x, phi1,
		xlabel="x",
		ylabel="Field Amplitude",
		legend=false,
	)
)
savefig("problem.png"); nothing # hide
```

![Refractive index structure and initial field](problem.png)

We now provide an effective (propagation) index `neff`
and propagate the field. We then plot the results:

```@example bpm2d
import FDBPM

neff=maximum(n)
phi=FDBPM.bpm(n, phi1, DeltaX, DeltaZ, neff)

plot(
	heatmap(z, x, abs.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Magnitude",
	),
	heatmap(z, x, angle.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Phase",
	)
)
savefig("propfield.png"); nothing # hide
```

![Results of BPM](propfield.png)

In order to obtain the complex propagated field we must add in the phase
of the propagated field (the results plotted above are only the magnitude
and phase relative to a propagating plane wave):

```@example bpm2d
phase=ones(Nx)*exp.(-im*z.*neff)'
phi.*=phase

plot(
	heatmap(z, x, abs.(phi),
		xlabel="x",
		ylabel="z",
		aspectratio=1,
		title="Magnitude",
	),
	heatmap(z, x, angle.(phi),
		xlabel="z",
		ylabel="x",
		aspectratio=1,
		title="Phase",
	)
)
savefig("propfield_corr.png"); nothing # hide
```

![Propagated field](propfield_corr.png)
