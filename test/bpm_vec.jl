DeltaX = DeltaZ = 0.1
Nx = 50
Nz = 100
n = ones(Nx, Nz)
u = ones(Nx, Nz)
n[div(Nx, 4):div(3 * Nx, 4), :] .= 1.5
neff = 1.5
phi1 = exp.(-LinRange(-5, 5, Nx) .^ 2)

@test (FDBPM.bpm(u, u, u, n, n, n, phi1, DeltaX, DeltaZ, neff); true)
phi = FDBPM.bpm(u, u, u, n, n, n, phi1, DeltaX, DeltaZ, neff)
@test size(phi) == size(n)
@test (
    FDBPM.bpm(
        Complex.(u),
        Complex.(u),
        Complex.(u),
        Complex.(n),
        Complex.(n),
        Complex.(n),
        phi1,
        DeltaX,
        DeltaZ,
        neff,
    );
    true
)
@test (FDBPM.bpm(u, u, u, n, n, n, Complex.(phi1), DeltaX, DeltaZ, neff); true)

DeltaX = DeltaY = DeltaZ = 0.1
Nx = 20
Ny = 30
Nz = 40
n = ones(Nx, Ny, Nz)
u = ones(Nx, Ny, Nz)
n[div(Nx, 4):div(3 * Nx, 4), div(Ny, 4):div(3 * Ny, 4), :] .= 1.5
neff = 1.5
phi1 = exp.(-LinRange(-5, 5, Nx) .^ 2) * exp.(-LinRange(-5, 5, Ny) .^ 2)'

@test (FDBPM.bpm(u, u, u, n, n, n, phi1, phi1, DeltaX, DeltaZ, neff); true)
phix, phiy = FDBPM.bpm(u, u, u, n, n, n, phi1, phi1, DeltaX, DeltaZ, neff)
@test size(phix) == size(phiy) == size(n)
@test (
    FDBPM.bpm(
        Complex.(u),
        Complex.(u),
        Complex.(u),
        Complex.(n),
        Complex.(n),
        Complex.(n),
        phi1,
        phi1,
        DeltaX,
        DeltaZ,
        neff,
    );
    true
)
@test (
    FDBPM.bpm(u, u, u, n, n, n, Complex.(phi1), Complex.(phi1), DeltaX, DeltaZ, neff); true
)
