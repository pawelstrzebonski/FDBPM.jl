DeltaX = DeltaZ = 0.1
Nx = 50
Nz = 100
n = ones(Nx, Nz)
n[div(Nx, 4):div(3 * Nx, 4), :] .= 1.5
neff = 1.5
phi1 = exp.(-LinRange(-5, 5, Nx) .^ 2)

@test (FDBPM.bpm(n, phi1, DeltaX, DeltaZ, neff); true)
phi = FDBPM.bpm(n, phi1, DeltaX, DeltaZ, neff)
@test size(phi) == size(n)
@test (FDBPM.bpm(n, Complex.(phi1), DeltaX, DeltaZ, neff); true)
@test (FDBPM.bpm(Complex.(n), phi1, DeltaX, DeltaZ, neff); true)

DeltaX = DeltaY = DeltaZ = 0.1
Nx = 50
Ny = 70
Nz = 100
n = ones(Nx, Ny, Nz)
n[div(Nx, 4):div(3 * Nx, 4), div(Ny, 4):div(3 * Ny, 4), :] .= 1.5
neff = 1.5
phi1 = exp.(-LinRange(-5, 5, Nx) .^ 2) * exp.(-LinRange(-5, 5, Ny) .^ 2)'

@test (FDBPM.bpm(n, phi1, DeltaX, DeltaY, DeltaZ, neff); true)
phi = FDBPM.bpm(n, phi1, DeltaX, DeltaY, DeltaZ, neff)
@test size(phi) == size(n)
@test (FDBPM.bpm(n, Complex.(phi1), DeltaX, DeltaY, DeltaZ, neff); true)
@test (FDBPM.bpm(Complex.(n), phi1, DeltaX, DeltaY, DeltaZ, neff); true)
