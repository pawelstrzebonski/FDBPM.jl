# FDBPM.jl Documentation

`FDBPM` is a package implementing the finite-difference beam propagation
method for simulating the propagation of electromagnetic fields.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDBPM.jl
```

## Features

* Support for 2D and 3D problems
* Implements FD-BPM of scalar field in region of isotropic refractive region
* Implements FD-BPM of vector field in region of anisotropic permeability and hyperactivity

## Status

*This is a work-in-progress and should be treated as such!*

The functionality has **not** been verified yet, so results should not
be trusted (indeed, the various implementations do not appear to give
quite the same results for the same inputs). Furthermore, the conventions
between the two implementations of FD-BPM in this package differ requiring
additional care in interpreting the results.
