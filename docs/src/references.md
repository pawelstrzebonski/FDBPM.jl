# References

The finite-difference beam propagation algorithms implemented here were
made by consulting a couple sources. The basic isotropic medium BPM
is based on the description of the method given in Chapter 7
(Section 7.4, Page 427) of
["Diode Lasers and Photonic Integrated Circuits"](https://coldren.ece.ucsb.edu/book).

The in-progress anisotropic implementation is based on the course notes for
EE 5337 CEM, as posted online, on BPM. These can be currently found in
[Lecture 5b](https://empossible.net/academics/emp5337/).
Also, the related
[poster](https://empossible.net/wp-content/uploads/2018/03/Poster_BPM.pdf)
may also be a good reference.
